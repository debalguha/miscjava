package com.test.app;

import com.test.app.postprocess.DataContentType;
import com.test.app.postprocess.dtos.MemberSyncResult;
import com.test.app.postprocess.dtos.MemberSyncResultBuilder;
import com.test.app.postprocess.startegies.PostProcessingStrategy;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = PostProcessingApplicationSpike.class)
public class PostProcessingApplicationTest {

    @Autowired
    @Qualifier("inMemoryProcessingStrategy")
    PostProcessingStrategy inMemoryProcessingStrategy;

    @Autowired
    @Qualifier("databaseRefreshProcessingStrategy")
    PostProcessingStrategy dbRefreshProcessingStrategy;

    @Test
    public void testInMemoryPostProcessingStrategy() {
        List<MemberSyncResult> memberSyncResults = inMemoryProcessingStrategy.postProcess(fj.data.List.arrayList(MemberSyncResultBuilder.dummy(RandomUtils.nextLong()).build()).toJavaList(), DataContentType.INSURANCE);
        Assert.assertNotNull(memberSyncResults);
        Assert.assertFalse(memberSyncResults.isEmpty());
        Assert.assertEquals(1, memberSyncResults.size());
        Assert.assertEquals(12, memberSyncResults.get(0).dataMap.size());
        memberSyncResults.stream().map(memberSyncResult -> memberSyncResult.dataMap.entrySet()).flatMap(entries -> entries.stream()).forEach(anEntry -> Assert.assertTrue(anEntry.getValue().endsWith("Processed!!")));
    }

    @Test
    public void testDBRefreshPostProcessingStrategy() {
        List<MemberSyncResult> memberSyncResults = dbRefreshProcessingStrategy.postProcess(fj.data.List.arrayList(MemberSyncResultBuilder.dummy(RandomUtils.nextLong()).build()).toJavaList(), DataContentType.INSURANCE);
        Assert.assertNotNull(memberSyncResults);
        Assert.assertFalse(memberSyncResults.isEmpty());
        Assert.assertEquals(1, memberSyncResults.size());
        Assert.assertEquals(12, memberSyncResults.get(0).dataMap.size());
        memberSyncResults.stream().map(memberSyncResult -> memberSyncResult.dataMap.entrySet()).flatMap(entries -> entries.stream()).forEach(anEntry -> Assert.assertTrue(anEntry.getValue().endsWith("Processed!!")));
    }
}
