package misc.tripA.event;

import java.util.EventListener;

public interface PageEventListener extends EventListener {
    void pageVisited(PageEvent pageEvent);
}
