package misc.tripA.event;

import java.util.EventObject;

public class PageEvent extends EventObject {
    public final PageEventType eventType;
    public final Long hotelId;
    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public PageEvent(Object source, PageEventType eventType, Long hotelId) {
        super(source);
        this.eventType = eventType;
        this.hotelId = hotelId;
    }
}
