package misc.tripA.impl;

import misc.tripA.Statistics;
import misc.tripA.VisitStat;
import misc.tripA.event.PageEvent;
import misc.tripA.event.PageEventListener;
import org.javatuples.Pair;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class Hotel implements PageEventListener {
    public final Long id;
    public final Collection<Pair<Lock, VisitStat>> visitStats;
    public Hotel(Long id){
        this.id = id;
        visitStats = new ArrayList<>(Statistics.values().length);
        initializeStats();
    }

    private void initializeStats() {
        for(Statistics stat : Statistics.values()){
            Pair<Lock, VisitStat> pair = new Pair<>(new ReentrantLock(), new ConcreteVisitStat(LocalDateTime.now().minusYears(100), stat, new AtomicLong(0)));
            visitStats.add(pair);
        }
    }


    @Override
    public void pageVisited(PageEvent pageEvent) {
        switch (pageEvent.eventType){
            case PAGE_VISTITED: runStatistiChecks(visitStats); break;
        }
    }

    protected Collection<Pair<Lock, VisitStat>> runStatistiChecks(Collection<Pair<Lock, VisitStat>> visitStats){
        return visitStats.stream().map(pair -> {
            pair.getValue0().lock();
            try {
                return new Pair(pair.getValue0(), pair.getValue1().timingStat().getVisitStatFunction().apply(pair.getValue1()));
            } finally {
                pair.getValue0().unlock();
            }
        }).collect(Collectors.toList());
    }
}
