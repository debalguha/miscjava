package misc.tripA.impl;

import misc.tripA.Statistics;
import misc.tripA.VisitStat;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class ConcreteVisitStat implements VisitStat {
    public final LocalDateTime lastVisited;
    public final Statistics timingStat;
    public final AtomicLong statValue;

    public ConcreteVisitStat(LocalDateTime lastVisited, Statistics timingStat, AtomicLong statValue) {
        this.lastVisited = lastVisited;
        this.timingStat = timingStat;
        this.statValue = statValue;
    }

    @Override
    public LocalDateTime lastVisited() {
        return lastVisited;
    }

    @Override
    public Statistics timingStat() {
        return timingStat;
    }

    @Override
    public AtomicLong statValue() {
        return statValue;
    }

    public static VisitStat seedStat(){
        return Statistics.NEVER.getVisitStatFunction().apply(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConcreteVisitStat that = (ConcreteVisitStat) o;
        return Objects.equals(statValue, that.statValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(statValue);
    }
}
