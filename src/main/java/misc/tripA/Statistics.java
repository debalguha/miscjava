package misc.tripA;

import misc.tripA.impl.ConcreteVisitStat;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;

public enum Statistics {
    PER_SEC("Visits Per 5 Minues", visitStat -> {
        LocalDateTime now = LocalDateTime.now();
        if(Duration.between(visitStat.lastVisited(), now).toMinutes()<=5){
            return new ConcreteVisitStat(now, Statistics.PER_SEC, new AtomicLong(visitStat.statValue().incrementAndGet()));
        } else {
            return visitStat;
        }
    }), PER_5_SEC("Visits Per 5 Minues", visitStat -> {
        LocalDateTime now = LocalDateTime.now();
        if(Duration.between(visitStat.lastVisited(), now).toHours()<=1){
            return new ConcreteVisitStat(now, Statistics.PER_5_SEC, new AtomicLong(visitStat.statValue().incrementAndGet()));
        } else {
            return visitStat;
        }
    }), PER_10_SEC("Visits Per 5 Minues", visitStat -> {
        LocalDateTime now = LocalDateTime.now();
        if(Duration.between(visitStat.lastVisited(), now).toHours()<=24){
            return new ConcreteVisitStat(now, Statistics.PER_10_SEC, new AtomicLong(visitStat.statValue().incrementAndGet()));
        } else {
            return visitStat;
        }
    }), NEVER("never Visited", visitStat ->{
        return new ConcreteVisitStat(LocalDateTime.now().minusYears(100), Statistics.NEVER, new AtomicLong(0));
    });

    String statName;
    Function<VisitStat, VisitStat> visitStatFunction;

    Statistics(String statName, Function<VisitStat, VisitStat> visitStatFunction){
        this.statName = statName;
        this.visitStatFunction = visitStatFunction;
    }

    public String getStatName() {
        return statName;
    }

    public Function<VisitStat, VisitStat> getVisitStatFunction() {
        return visitStatFunction;
    }

}
