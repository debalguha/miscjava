package misc.tripA;

public interface WithPageVisitStatistics {
    void addPageVisitStatistics(VisitStat visitStat);
}
