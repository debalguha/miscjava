package misc.tripA;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicLong;

public interface VisitStat {
    LocalDateTime lastVisited();
    Statistics timingStat();
    AtomicLong statValue();
}
