package juel;

import de.odysseus.el.util.SimpleContext;

import javax.el.ExpressionFactory;
import java.lang.reflect.Method;

public class JuelExpressionEnricherEvaluator {

    public static ExpressionFactory createExpressionFactory(){
        return new de.odysseus.el.ExpressionFactoryImpl();
    }

    public static SimpleContext createContext(){
        return new SimpleContext();
    }

    public static <T> SimpleContext setVariableInContext(String varName, T value, SimpleContext context, Class<T> valClass){
        context.setVariable(varName, ExpressionFactory.newInstance().createValueExpression(value, valClass));
        return context;
    }

    public static SimpleContext setFunction(String nameSpace, String functionName, Method method, SimpleContext context){
        context.setFunction(nameSpace, functionName, method);
        return context;
    }

    public static <T> Object executeValueExpressionInContext(String expression, SimpleContext context, Class<T> valClass) {
        return createExpressionFactory().createValueExpression(context, expression, valClass).getValue(context);
    }
}
