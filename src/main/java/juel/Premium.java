package juel;

import java.time.LocalDate;
import java.util.Date;

public class Premium {
    private Double premiumAmount;
    private LocalDate premiumStartDate;
    private LocalDate premiumEndDate;

    public Premium(Double premiumAmount, LocalDate premiumStartDate, LocalDate premiumEndDate) {
        this.premiumAmount = premiumAmount;
        this.premiumStartDate = premiumStartDate;
        this.premiumEndDate = premiumEndDate;
    }

    public Double getPremiumAmount() {
        return premiumAmount;
    }

    public LocalDate getPremiumStartDate() {
        return premiumStartDate;
    }

    public LocalDate getPremiumEndDate() {
        return premiumEndDate;
    }
}
