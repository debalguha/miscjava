package com.test.app.repository;

import com.test.app.postprocess.dtos.InsuranceCover;
import com.test.app.postprocess.dtos.MemberSyncResult;
import com.test.app.postprocess.dtos.PvsMember;
import fj.P2;

import java.util.Collection;

public interface DataBackendRepository {
    P2<PvsMember, Collection<InsuranceCover>> refreshData(MemberSyncResult result);
}
