package com.test.app.repository;

import com.test.app.postprocess.dtos.DummyInsuranceDataProvider;
import com.test.app.postprocess.dtos.InsuranceCover;
import com.test.app.postprocess.dtos.InsuranceCoverBuilder;
import com.test.app.postprocess.dtos.MemberSyncResult;
import com.test.app.postprocess.dtos.PvsMember;
import fj.P;
import fj.P2;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
public class DummyDataBackendRepositoryImpl implements DataBackendRepository {
    @Override
    public P2<PvsMember, Collection<InsuranceCover>> refreshData(MemberSyncResult result) {
        return P.p(result.data, DummyInsuranceDataProvider.create4InsuranceCover(result.data.pvsMemberId));
    }
}
