package com.test.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostProcessingApplicationSpike {
    public  static void main(String args[]) { SpringApplication.run(PostProcessingApplicationSpike.class, args); }
}
