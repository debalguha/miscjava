package com.test.app.postprocess;

public enum DataContentType {
    INSURANCE, CONTRIBUTION;
}
