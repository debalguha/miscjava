package com.test.app.postprocess.startegies;

import com.test.app.converter.LIDEXOutboundConverter;
import com.test.app.postprocess.DataContentType;
import com.test.app.postprocess.ProcessorFactory;
import com.test.app.postprocess.dtos.MemberSyncResult;
import com.test.app.repository.DataBackendRepository;
import fj.P;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component("databaseRefreshProcessingStrategy")
public class DatabaseRefreshProcessingStrategy implements PostProcessingStrategy, InitializingBean {

    private ProcessorFactory processorFactory;
    private DataBackendRepository repository;

    public DatabaseRefreshProcessingStrategy(@Autowired ProcessorFactory processorFactory, @Autowired DataBackendRepository repository) {
        this.processorFactory = processorFactory;
        this.repository = repository;
    }
    @Override
    public List<MemberSyncResult> postProcess(List<MemberSyncResult> memberSyncResults, DataContentType contentType) {
        return memberSyncResults.stream().map(memberSyncResult -> repository.refreshData(memberSyncResult)).map(pvsMemberCollectionP2 -> {
            Map<String, String> insuranceMap = processorFactory.obtainPostProcessorForContentType(contentType).process(LIDEXOutboundConverter.convert(pvsMemberCollectionP2._2()));
            return P.p(pvsMemberCollectionP2._1(), insuranceMap);
        }).map(aPair -> new MemberSyncResult(aPair._1(), aPair._2())).collect(Collectors.toList());
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
