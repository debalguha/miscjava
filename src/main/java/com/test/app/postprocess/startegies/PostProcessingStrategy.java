package com.test.app.postprocess.startegies;

import com.test.app.postprocess.DataContentType;
import com.test.app.postprocess.dtos.MemberSyncResult;

import java.util.List;

public interface PostProcessingStrategy {
    List<MemberSyncResult> postProcess(List<MemberSyncResult> memberSyncResults, DataContentType contentType);
}
