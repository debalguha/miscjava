package com.test.app.postprocess.startegies;

import com.test.app.postprocess.DataContentType;
import com.test.app.postprocess.ProcessorFactory;
import com.test.app.postprocess.dtos.MemberSyncResult;
import fj.P;
import fj.P2;
import fj.data.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component("inMemoryProcessingStrategy")
public class InMemoryProcessingStrategy implements PostProcessingStrategy {

    private ProcessorFactory processorFactory;

    private Function<Map.Entry<Long, List<MemberSyncResult>>, MemberSyncResult> transformMemberSyncresultMap = (longListEntry ->{
        MemberSyncResult syncResult = longListEntry.getValue().stream().reduce((memberSyncResult, memberSyncResult2) -> combineMemberSyncResult(memberSyncResult, memberSyncResult2)).get();
        return syncResult;
    });
    public InMemoryProcessingStrategy(@Autowired ProcessorFactory processorFactory) {
        this.processorFactory = processorFactory;
    }

    @Override
    public List<MemberSyncResult> postProcess(List<MemberSyncResult> memberSyncResults, DataContentType contentType) {
        return memberSyncResults.stream().collect(Collectors.groupingBy(memberSyncResult -> memberSyncResult.data.pvsMemberId)).entrySet().stream()
                .map(longListEntry -> transformMemberSyncresultMap.apply(longListEntry)).map(memberSyncResult -> new MemberSyncResult(memberSyncResult.data, processorFactory.obtainPostProcessorForContentType(contentType).process(memberSyncResult.dataMap))
        ).collect(Collectors.toList());
    }

    public Map<String, String> combineMap(List<Map<String, String>> inputMapList) {
        return inputMapList.stream().reduce((map1, map2) -> {
            map1.putAll(map2); return map1;
        }).get();
    }

    public MemberSyncResult combineMemberSyncResult(MemberSyncResult result1, MemberSyncResult result2) {
        return new MemberSyncResult(result1.data, combineMap(Array.array(result1.dataMap, result2.dataMap).asJavaList()));
    }

}
