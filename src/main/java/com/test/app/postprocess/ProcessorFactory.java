package com.test.app.postprocess;

import com.test.app.postprocess.processor.ContributionMapProcessor;
import com.test.app.postprocess.processor.InsuranceMapProcesor;
import com.test.app.postprocess.processor.MapProcessor;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ProcessorFactory implements ApplicationContextAware {
    private ApplicationContext appCtx;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.appCtx = applicationContext;
    }
    public MapProcessor obtainPostProcessorForContentType(DataContentType contentType){
        switch (contentType) {
            case INSURANCE: return appCtx.getBean(InsuranceMapProcesor.class);
            case CONTRIBUTION: return appCtx.getBean(ContributionMapProcessor.class);
        }
        return null;
    }
}
