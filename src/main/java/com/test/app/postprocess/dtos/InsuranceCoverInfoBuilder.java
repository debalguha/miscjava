package com.test.app.postprocess.dtos;

public final class InsuranceCoverInfoBuilder {
    public String coverAmount;
    public CoverType coverType;

    private InsuranceCoverInfoBuilder() {
    }

    public static InsuranceCoverInfoBuilder anInsuranceCoverInfo() {
        return new InsuranceCoverInfoBuilder();
    }

    public InsuranceCoverInfoBuilder withCoverAmount(String coverAmount) {
        this.coverAmount = coverAmount;
        return this;
    }

    public InsuranceCoverInfoBuilder withCoverType(CoverType coverType) {
        this.coverType = coverType;
        return this;
    }

    public InsuranceCoverInfo build() {
        return new InsuranceCoverInfo(coverAmount, coverType);
    }

}
