package com.test.app.postprocess.dtos;

public class InsuranceCoverInfo {
    public String coverAmount;
    public CoverType coverType;
    public InsuranceCoverInfo(String coverAmount, CoverType coverType) {
        this.coverAmount = coverAmount;
        this.coverType = coverType;
    }
}
