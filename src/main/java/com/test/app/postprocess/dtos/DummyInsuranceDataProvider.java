package com.test.app.postprocess.dtos;

import org.apache.commons.lang3.RandomUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class DummyInsuranceDataProvider {
    public static Collection<InsuranceCoverInfo> create3CoverInfo(){
        return Arrays.stream(CoverType.values()).map(coverType -> InsuranceCoverInfoBuilder.anInsuranceCoverInfo().withCoverType(coverType)
                .withCoverAmount(String.valueOf(RandomUtils.nextDouble())).build())
                .collect(Collectors.toList());
    }

    public static Collection<InsuranceCover> create4InsuranceCover(Long pvsMemberId) {
        return Arrays.stream(RiskType.values()).map(riskType -> InsuranceCoverBuilder.anInsuranceCover()
                .withCoverInfos(create3CoverInfo()).withRiskType(riskType).withPvsMemberId(pvsMemberId).build())
                .collect(Collectors.toList());
    }
}
