package com.test.app.postprocess.dtos;

import com.test.app.converter.LIDEXOutboundConverter;
import org.apache.commons.lang3.RandomUtils;

import java.util.Map;

public final class MemberSyncResultBuilder {
    public PvsMember data;
    public Map<String, String> dataMap;

    private MemberSyncResultBuilder() {
    }

    public static MemberSyncResultBuilder aMemberSyncResult() {
        return new MemberSyncResultBuilder();
    }

    public MemberSyncResultBuilder withData(PvsMember data) {
        this.data = data;
        return this;
    }

    public MemberSyncResultBuilder withDataMap(Map<String, String> dataMap) {
        this.dataMap = dataMap;
        return this;
    }

    public MemberSyncResult build() {
        return new MemberSyncResult(data, dataMap);
    }

    public static MemberSyncResultBuilder dummy(Long pvsMemberId){
        return aMemberSyncResult().withData(new PvsMember(pvsMemberId)).withDataMap(LIDEXOutboundConverter.convert(DummyInsuranceDataProvider.create4InsuranceCover(pvsMemberId)));
    }
}
