package com.test.app.postprocess.dtos;

import java.util.Collection;

public final class InsuranceCoverBuilder {
    public Long pvsMemberId;
    public RiskType riskType;
    public Collection<InsuranceCoverInfo> coverInfos;

    private InsuranceCoverBuilder() {
    }

    public static InsuranceCoverBuilder anInsuranceCover() {
        return new InsuranceCoverBuilder();
    }

    public static InsuranceCoverBuilder fromInsuranceCover(InsuranceCover cover) {
        return anInsuranceCover().withRiskType(cover.riskType).withPvsMemberId(cover.pvsMemberId).withCoverInfos(cover.coverInfos);
    }

    public InsuranceCoverBuilder withPvsMemberId(Long pvsMemberId) {
        this.pvsMemberId = pvsMemberId;
        return this;
    }

    public InsuranceCoverBuilder withRiskType(RiskType riskType) {
        this.riskType = riskType;
        return this;
    }

    public InsuranceCoverBuilder withCoverInfos(Collection<InsuranceCoverInfo> coverInfos) {
        this.coverInfos = coverInfos;
        return this;
    }

    public InsuranceCover build() {
        return new InsuranceCover(pvsMemberId, riskType, coverInfos);
    }
}
