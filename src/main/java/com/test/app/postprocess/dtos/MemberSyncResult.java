package com.test.app.postprocess.dtos;

import java.util.Map;

public class MemberSyncResult {
    public PvsMember data;
    public Map<String, String> dataMap;

    public MemberSyncResult(PvsMember data, Map<String, String> dataMap) {
        this.data = data;
        this.dataMap = dataMap;
    }
}
