package com.test.app.postprocess.dtos;

import java.util.Collection;

public class InsuranceCover {
    public Long pvsMemberId;
    public RiskType riskType;
    public Collection<InsuranceCoverInfo> coverInfos;

    public InsuranceCover(Long pvsMemberId, RiskType riskType, Collection<InsuranceCoverInfo> coverInfos) {
        this.coverInfos = coverInfos;
        this.pvsMemberId = pvsMemberId;
        this.riskType = riskType;
    }
}
