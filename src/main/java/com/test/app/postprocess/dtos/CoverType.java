package com.test.app.postprocess.dtos;

public enum CoverType {

    DEFAULT ( "Default"),
    DEFAULT_ADDITIONAL ( "DefaultAdd"),
    VOLUNTARY ( "Vol");
    
    private final String shortName;

    private CoverType(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }
}
