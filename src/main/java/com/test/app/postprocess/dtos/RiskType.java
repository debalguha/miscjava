package com.test.app.postprocess.dtos;

public enum RiskType {

    LIFE ( "life" ),
    TPD ( "tpd" ),
    INCOME_PROTECTION ( "ip" ),
    SCB ( "scb" );
    
    private final String shortName;
    
    private RiskType(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }
}
