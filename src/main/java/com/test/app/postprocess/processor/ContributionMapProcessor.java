package com.test.app.postprocess.processor;

import java.util.Map;

public class ContributionMapProcessor extends AbstractMapProcessor {
    @Override
    public Map<String, String> process(Map<String, String> dataMap) {
        return superProcess(dataMap);
    }
}
