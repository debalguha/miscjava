package com.test.app.postprocess.processor;

import fj.P;
import fj.P2;

import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractMapProcessor implements MapProcessor {
    protected Map<String, String> superProcess(Map<String, String> dataMap) {
        return dataMap.entrySet().stream().map(anEntry -> P.p(anEntry.getKey(), String.format("%s:-%s", anEntry.getValue(), "Processed!!"))).collect(Collectors.toMap(P2:: _1, P2:: _2));
    }
}
