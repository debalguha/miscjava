package com.test.app.postprocess.processor;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class InsuranceMapProcesor extends AbstractMapProcessor {
    @Override
    public Map<String, String> process(Map<String, String> dataMap) {
        return superProcess(dataMap);
    }
}
