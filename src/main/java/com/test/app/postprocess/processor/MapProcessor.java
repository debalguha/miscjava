package com.test.app.postprocess.processor;

import java.util.Map;

public interface MapProcessor extends Processor<Map<String, String>> {
    Map<String, String> process(Map<String, String> dataMap);
}
