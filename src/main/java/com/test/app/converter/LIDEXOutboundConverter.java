package com.test.app.converter;

import com.test.app.postprocess.dtos.CoverType;
import com.test.app.postprocess.dtos.RiskType;
import com.test.app.postprocess.dtos.InsuranceCover;
import com.test.app.postprocess.dtos.InsuranceCoverInfo;
import fj.P;
import fj.P2;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class LIDEXOutboundConverter {
/*    public Map<Long, Collection<InsuranceCover>> convertInsCoversToMemberIdInsuranceCoverMap(Collection<InsuranceCover> insuranceCovers) {
        return insuranceCovers.stream().collect(Collectors.groupingBy(insuranceCover -> insuranceCover.));
    }*/

    public static Map<String, String> convert(Collection<InsuranceCover> coversForMember) {
        return coversForMember.stream().map(
                insuranceCover -> insuranceCover.coverInfos.stream().map(insuranceCoverInfo -> Arrays.stream(InsuranceCoverInfo.class.getDeclaredFields())
                        .filter(field -> !field.getName().equals("coverType"))
                        .map(field -> getFieldNameAndValueAsPair(insuranceCover.riskType, field, insuranceCoverInfo)).collect(Collectors.toMap(P2::_1, P2::_2))
                ).reduce((stringStringMap, stringStringMap2) -> {
                    stringStringMap.putAll(stringStringMap2);
                    return stringStringMap;
                }).get()
        ).reduce((stringStringMap, stringStringMap2) -> {
            stringStringMap.putAll(stringStringMap2);
            return stringStringMap;
        }).get();
    }

    private static P2<String, String> getFieldNameAndValueAsPair(RiskType riskType, Field field, InsuranceCoverInfo insuranceCoverInfo) {
        try {
            return P.p(createName(riskType, insuranceCoverInfo.coverType, field.getName()), field.get(insuranceCoverInfo).toString());
        } catch (IllegalAccessException e) {
           throw new RuntimeException(e);
        }
    }
    private static String createName(RiskType type, CoverType coverType, String suffix) {
        return String.format("Cover.%s%s%s", type.getShortName(),coverType.getShortName(), suffix);
    }
}
